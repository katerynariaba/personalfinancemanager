﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PersonalFinanceManager.Model.DbServices;
using PersonalFinanceManager.Model.DomainModel;

namespace PersonalFinanceManager.UnitTest
{
    [TestClass]
    public class UserDbServiceUnitTest
    {
        [TestMethod]
        public void LoginMethodReturnTrueForExistingUser()
        {
            var service = new UserDbService();

            var user = new User()
            {
                LastName = "admin",
                FirstName = "admin",
                Email = "admin@gmail.com",
                Password = "admin"
            };
            service.Add(user);

            var result = service.Login(user);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void LoginMethodReturnFalseForNotExistingUser()
        {
            var service = new UserDbService();

            var user = new User()
            {
                LastName = "user",
                FirstName = "user",
                Email = "user@gmail.com",
                Password = "user"
            };

            var result = service.Login(user);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void AddingMethodAddUser()
        {
            var service = new UserDbService();

            var user = new User()
            {
                LastName = "LastName",
                FirstName = "FirstName",
                Email = "email@gmail.com",
                Password = "password"
            };
            service.Add(user);

            var result = service.Login(user);

            Assert.IsTrue(result);

            service.DeleteUser(user);
        }
    }
}
