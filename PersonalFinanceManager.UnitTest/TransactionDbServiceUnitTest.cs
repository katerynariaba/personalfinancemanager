﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PersonalFinanceManager.Model.DbServices;
using PersonalFinanceManager.Model.DomainModel;

namespace PersonalFinanceManager.UnitTest
{
    [TestClass]
    public class TransactionDbServiceUnitTest
    {
        [TestMethod]
        public void GetTransactionMethodReturnTrueData()
        {
            var service = new TransactionDbService();

            var result = service.GetTransaction(12, 1, 3500, new DateTime(2019, 05, 01));

            Assert.AreEqual(12, result.CategoryId);
            Assert.AreEqual(1, result.UserId);
            Assert.AreEqual(3500, result.Value);
            Assert.AreEqual(new DateTime(2019, 05, 01), result.Date);

        }

        [TestMethod]
        public void GetTransactionMethodReturnIsNotNull()
        {
            var service = new TransactionDbService();

            var result = service.GetTransaction(12, 1, 3500, new DateTime(2019, 05, 01));

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddingMethodAddTransaction()
        {
            var service = new TransactionDbService();

            var transaction = new Transaction() {
                UserId = 1, Value = 1, CategoryId = 1, Date=DateTime.Today
            };
            service.Add(transaction);

            Assert.AreEqual(1, transaction.CategoryId);
            Assert.AreEqual(1, transaction.UserId);
            Assert.AreEqual(1, transaction.Value);
            Assert.AreEqual(DateTime.Today, transaction.Date);

            service.DeleteTransaction(transaction);
        }

        [TestMethod]
        public void GetFromPeriodTransactionReturnNotEmptyList()
        {
            var service = new TransactionDbService();
            var result = service
                .GetFromPeriodTransaction(new DateTime(2019,05,01), new DateTime(2019,06,12), 1);

            CollectionAssert.AllItemsAreNotNull(result);
        }

        [TestMethod]
        public void GetAllTransactionReturnNotEmptyListFor1()
        {
            var service = new TransactionDbService();
            var result = service.GetAllTransaction(1);

            CollectionAssert.AllItemsAreNotNull(result);
        }

        [TestMethod]
        public void SummaryByCategoryReturnNotEmptyList()
        {
            var service = new TransactionDbService();
            var result = service
                .SummaryByCategory(new DateTime(2019, 05, 01), new DateTime(2019, 06, 12), "Expense", 11);

            CollectionAssert.AllItemsAreNotNull(result);
        }

        [TestMethod]
        public void SummaryAllByCategoryReturnNotEmptyList()
        {
            var service = new TransactionDbService();
            var result = service
                .SummaryAllByCategory("Expense", 11);

            CollectionAssert.AllItemsAreNotNull(result);
        }

        [TestMethod]
        public void GetBalanceByPeriodReturnDecimal()
        {
            var service = new TransactionDbService();
            var result = service
                .GetBalanceByPeriod(new DateTime(2019, 05, 01), new DateTime(2019, 06, 12), "Expense", 11);

            Assert.IsInstanceOfType(result, typeof(decimal));
        }

        [TestMethod]
        public void GetBalanceByTypeReturnDecimal()
        {
            var service = new TransactionDbService();
            var result = service
                .GetBalanceByType("Expense", 11);

            Assert.IsInstanceOfType(result, typeof(decimal));
        }

        [TestMethod]
        public void GetMinDateReturnDateTime()
        {
            var service = new TransactionDbService();
            var result = service
                .GetMinDate(1);

            Assert.IsInstanceOfType(result, typeof(DateTime));
        }

        [TestMethod]
        public void GetMaxDateReturnDateTime()
        {
            var service = new TransactionDbService();
            var result = service
                .GetMaxDate(1);

            Assert.IsInstanceOfType(result, typeof(DateTime));
        }
    }
}
