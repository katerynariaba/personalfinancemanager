﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PersonalFinanceManager.Model.Helpers;
using PersonalFinanceManager.Model.DbServices;
using PersonalFinanceManager.Model.Model;
using System.Collections.Generic;

namespace PersonalFinanceManager.UnitTest
{
    [TestClass]
    public class ValidationHelperUnitTest
    {
        [TestMethod]
        public void EmailValidMethodReturnTrueForValidEmail()
        {
            var helper = new ValidationHelper();

            var result = helper.EmailIsValid("email@gmail.com");

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void EmailValidMethodReturnFalseForNotValidEmail()
        {
            var helper = new ValidationHelper();

            var result = helper.EmailIsValid("email");

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void EmailValidMethodReturnBool()
        {
            var helper = new ValidationHelper();

            var result = helper.EmailIsValid("email");

            Assert.IsInstanceOfType(result, typeof(bool));
        }

        [TestMethod]
        public void ValueValidMethodReturnTrueFor5()
        {
            var helper = new ValidationHelper();

            var result = helper.ValueIsValid("5");

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValueValidMethodReturnTrueForFraction()
        {
            var helper = new ValidationHelper();

            var result = helper.ValueIsValid("7.25");

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValueValidMethodReturnFalseForText()
        {
            var helper = new ValidationHelper();

            var result = helper.ValueIsValid("azaza");

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ValueValidMethodReturnBool()
        {
            var helper = new ValidationHelper();

            var result = helper.ValueIsValid("7.25");

            Assert.IsInstanceOfType(result, typeof(bool));
        }
    }
}
