﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PersonalFinanceManager.Model.DbServices;
using PersonalFinanceManager.Model.DomainModel;
using System.Runtime.Remoting.Contexts;
using System.Data.Entity;
using PersonalFinanceManager.Model.DB;
using PersonalFinanceManager.Model.Model;
using System.Collections.Generic;

namespace PersonalFinanceManager.UnitTest
{
    [TestClass]
    public class CategoryDbServiceUnitTest
    {
        [TestMethod]
        public void GetCategoryByTitleMethodReturnTrueTitle()
        {
            var service = new CategoryDbService();

            var result = service.GetCategoryByTitle("Salary");

            Assert.AreEqual("Salary", result.Title);
        }

        [TestMethod]
        public void GetCategoryByTitleMethodReturnIsNotNull()
        {
            var service = new CategoryDbService();

            var result = service.GetCategoryByTitle("Salary");

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddingMethodAddCategory()
        {
            var service = new CategoryDbService();

            var category = new Category() { Title = "Title", Type = "Type" };
            service.Add(category);
            var result = service.GetCategoryByTitle("Title");

            Assert.AreEqual("Title", result.Title);

            service.DeleteCategory(category);
        }

        [TestMethod]
        public void GetAllCategoryReturnNotEmptyList()
        {
            var service = new CategoryDbService();
            var result = service.GetAllCategory("Expense");

            CollectionAssert.AllItemsAreNotNull(result);
        }

        [TestMethod]
        public void CategoryIdReturnNotNull()
        {
            var service = new CategoryDbService();
            var result = service.GetCategoryIdByTytle("Car");

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void CategoryIdReturnInt()
        {
            var service = new CategoryDbService();
            var result = service.GetCategoryIdByTytle("Car");

            Assert.IsInstanceOfType(result, typeof(int));
        }

        [TestMethod]
        public void CategoryIdReturn68ForCar()
        {
            var service = new CategoryDbService();
            var result = service.GetCategoryIdByTytle("Car");

            Assert.AreEqual(68, result);
        }
    }
}
