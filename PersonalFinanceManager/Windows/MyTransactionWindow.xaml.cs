﻿using PersonalFinanceManager.Model.DbServices;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System;
using PersonalFinanceManager.Model.Model;

namespace PersonalFinanceManager
{
    /// <summary>
    /// Interaction logic for MyTransactionWindow.xaml
    /// </summary>
    public partial class MyTransactionWindow : Window
    {

        public MyTransactionWindow()
        {
            InitializeComponent();

            List<string> categoriesTime = new List<string>() { "All time", "Period", "Last year", "Last month", "Last week", "Current day" };

            timeForTransactionComboBox.ItemsSource = categoriesTime;
            timeForTransactionComboBox.SelectedIndex = 5;

            List<string> categoriesType = new List<string>() { "Expense", "Income" };

            typeComboBox.ItemsSource = categoriesType;
            typeComboBox.Visibility = Visibility;
            fromTransactionDate.SelectedDate = DateTime.Today;
            toTransactionDate.SelectedDate = DateTime.Today;
            RefreshTransactions();
        }

        private void RefreshTransactions()
        {
            if (fromTransactionDate.SelectedDate != null && toTransactionDate.SelectedDate != null)
            {
                DateTime dateFrom = fromTransactionDate.SelectedDate.Value;
                DateTime dateTo = toTransactionDate.SelectedDate.Value;

                var summaryByCategory = new TransactionDbService()
                    .SummaryByCategory(dateFrom, dateTo, typeComboBox.Text, UserData.Id);
                var source = new ObservableCollection<TransactionsSummary>(summaryByCategory);
                transactionDataGrid.ItemsSource = source;


                Dictionary<string, decimal> valueList = new Dictionary<string, decimal>();
                foreach (var item in summaryByCategory)
                {
                    valueList.Add(item.Category, item.Summary);
                }
                pieChart.DataContext = valueList;

            }
        }

        private void timeForTransactionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime dateFrom = DateTime.Today;
            DateTime dateTo = DateTime.Today;
            bool defaultMode = true;
            dateFromTransactionLabel.Content = string.Empty;
            dateToTransactionLabel.Content = string.Empty;

            switch (timeForTransactionComboBox.SelectedItem.ToString())
            {
                case "Last year":
                    fromTransactionDate.Visibility = Visibility.Collapsed;
                    toTransactionDate.Visibility = Visibility.Collapsed;

                    dateFrom = new DateTime(DateTime.Today.Year, 1, 1);
                    dateTo = new DateTime(DateTime.Today.Year, 12, 31);

                    break;
                case "All time":
                    fromTransactionDate.Visibility = Visibility.Collapsed;
                    toTransactionDate.Visibility = Visibility.Collapsed;

                    defaultMode = false;

                    DateTime? dateMin = new TransactionDbService().GetMinDate(UserData.Id);
                    DateTime? dateMax = new TransactionDbService().GetMaxDate(UserData.Id);

                    dateFromTransactionLabel.Content = dateMin == null ? string.Empty :
                        ((DateTime)dateMin).ToShortDateString();
                    dateToTransactionLabel.Content = dateMax == null ? string.Empty :
                        ((DateTime)dateMax).ToShortDateString();

                    var summaryByCategory = new TransactionDbService()
                    .SummaryAllByCategory(typeComboBox.Text, UserData.Id);
                    var source = new ObservableCollection<TransactionsSummary>(summaryByCategory);
                    transactionDataGrid.ItemsSource = source;

                    List<KeyValuePair<string, decimal>> valueList = new List<KeyValuePair<string, decimal>>();
                    foreach (var item in summaryByCategory)
                    {
                        valueList.Add(new KeyValuePair<string, decimal>(item.Category, item.Summary));
                    }
                    pieChart.DataContext = valueList;

                    break;
                case "Period":
                    fromTransactionDate.Visibility = Visibility.Visible;
                    toTransactionDate.Visibility = Visibility.Visible;

                    defaultMode = false;
                    break;
                case "Last month":
                    fromTransactionDate.Visibility = Visibility.Collapsed;
                    toTransactionDate.Visibility = Visibility.Collapsed;

                    dateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                    dateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month + 1, 1).AddDays(-1);
                    break;
                case "Last week":
                    fromTransactionDate.Visibility = Visibility.Collapsed;
                    toTransactionDate.Visibility = Visibility.Collapsed;

                    dateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).AddDays(-7);
                    dateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                    break;
                case "Current day":
                    fromTransactionDate.Visibility = Visibility.Collapsed;
                    toTransactionDate.Visibility = Visibility.Collapsed;

                    dateFrom = DateTime.Today;
                    dateTo = DateTime.Today;
                    break;
            }

            if (defaultMode)
            {
                dateFromTransactionLabel.Content = dateFrom.ToShortDateString();
                dateToTransactionLabel.Content = dateTo.ToShortDateString();

                var summaryByCategory = new TransactionDbService()
                    .SummaryByCategory(dateFrom, dateTo, typeComboBox.Text, UserData.Id);

                var source = new ObservableCollection<TransactionsSummary>(summaryByCategory);
                transactionDataGrid.ItemsSource = source;

                Dictionary<string, decimal> valueList = new Dictionary<string, decimal>();
                foreach (var item in summaryByCategory)
                {
                    valueList.Add(item.Category, item.Summary);
                }
                pieChart.DataContext = valueList;
            }
        }

        private void fromTransactionDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            RefreshTransactions();
        }

        private void toTransactionDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            RefreshTransactions();
        }
        
    }
}
