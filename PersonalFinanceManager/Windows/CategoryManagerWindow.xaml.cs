﻿using PersonalFinanceManager.Model.DB;
using PersonalFinanceManager.Model.DbServices;
using PersonalFinanceManager.Model.DomainModel;
using System.Collections.Generic;
using System.Runtime.Remoting.Contexts;
using System.Windows;

namespace PersonalFinanceManager
{
    /// <summary>
    /// Interaction logic for CategoryManagerWindow.xaml
    /// </summary>
    public partial class CategoryManagerWindow : Window
    {
        public CategoryManagerWindow()
        {
            InitializeComponent();

            List<string> categoriesType = new List<string>() { "Expense", "Income" };

            typeCategoryComboBox.ItemsSource = categoriesType;
            typeCategoryComboBox.SelectedIndex = 0;
            typeCategoryComboBoxEd.ItemsSource = categoriesType;
            typeCategoryComboBoxEd.SelectedIndex = 0;
            newTypeCategoryComboBox.ItemsSource = categoriesType;
            newTypeCategoryComboBox.SelectedIndex = 0;
            typeCategoryComboBoxDel.ItemsSource = categoriesType;
            typeCategoryComboBoxDel.SelectedIndex = 0;

            RefreshCategories("Expense");
        }

        public void RefreshCategories(string type)
        {
            var categories = new CategoryDbService().GetAllCategory(type);

            categoryComboBox.ItemsSource = categories;
            categoryComboBox.DisplayMemberPath = "Title";
            categoryComboBox.SelectedValuePath = "Id";
            categoryComboBox.SelectedIndex = 0;

            categoryComboBoxDel.ItemsSource = categories;
            categoryComboBoxDel.DisplayMemberPath = "Title";
            categoryComboBoxDel.SelectedValuePath = "Id";
            categoryComboBoxDel.SelectedIndex = 0;
        }

        private void categoryAddButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(titleCategoryBox.Text))
            {
                MessageBox.Show("Enter a title.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                titleCategoryBox.Focus();
            }
            else
            {
                new CategoryDbService().Add(new Category()
                {
                    Title = titleCategoryBox.Text,
                    Type = typeCategoryComboBox.Text
                });

                MessageBox.Show("New category successfully added.", "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void typeCategoryComboBoxEd_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            string type = typeCategoryComboBoxEd.SelectedValue.ToString();
            RefreshCategories(type);
        }

        private void editCategoryButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(titleCategoryBoxEd.Text))
            {
                MessageBox.Show("Enter a title.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                titleCategoryBox.Focus();
            }
            else
            {
                new CategoryDbService().Add(new Category()
                {
                    Title = titleCategoryBoxEd.Text,
                    Type = newTypeCategoryComboBox.SelectedValue.ToString()
                });

                Category oldCategory = new CategoryDbService()
                .GetCategoryByTitle(categoryComboBox.Text);
                using (FinanceDbContext context = new FinanceDbContext())
                {
                    context.Categories.Attach(oldCategory);
                    context.Categories.Remove(oldCategory);
                    context.SaveChanges();
                }

                MessageBox.Show("Category successfully edited.", "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void typeCategoryComboBoxDel_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            string type = typeCategoryComboBoxDel.SelectedValue.ToString(); ;
            RefreshCategories(type);
        }

        private void сategoryDeleteButtonDel_Click(object sender, RoutedEventArgs e)
        {
            Category category = new CategoryDbService()
                .GetCategoryByTitle(categoryComboBoxDel.Text);
            using (FinanceDbContext context = new FinanceDbContext())
            {
                context.Categories.Attach(category);
                context.Categories.Remove(category);
                context.SaveChanges();
            }
            MessageBox.Show("Category successfully deleted.", "", MessageBoxButton.OK, MessageBoxImage.Information);
        }
        
        
    }
}

