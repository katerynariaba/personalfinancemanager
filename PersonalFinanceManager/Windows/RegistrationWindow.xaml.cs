﻿using PersonalFinanceManager.Model.DbServices;
using PersonalFinanceManager.Model.DomainModel;
using PersonalFinanceManager.Model.Helpers;
using System.Windows;

namespace PersonalFinanceManager
{
    /// <summary>
    /// Interaction logic for RegistrationWindow.xaml
    /// </summary>

    public partial class RegistrationWindow : Window
    {
        public RegistrationWindow()
        {
            InitializeComponent();
        }

        private void logInButton_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow login = new LoginWindow();
            login.Show();
            Close();
        }

        private void singUpButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(emailRegistrBox.Text))
            {
                MessageBox.Show("Enter an email.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                emailRegistrBox.Focus();
            }
            else if (!new ValidationHelper().EmailIsValid(emailRegistrBox.Text))
            {
                MessageBox.Show("Enter a valid email.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                emailRegistrBox.Focus();
            }
            else
            {
                if (string.IsNullOrEmpty(passwordRegistrBox.Password))
                {
                    MessageBox.Show("Enter password.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    passwordRegistrBox.Focus();
                }
                else if (passwordRegistrBox.Password != passwordConfirmBox.Password)
                {
                    MessageBox.Show("Confirm password must be same as password.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    passwordConfirmBox.Focus();
                }
                else
                {
                    new UserDbService().Add(new User()
                    {
                        LastName = lastNameBox.Text,
                        FirstName = firstNameBox.Text,
                        Email = emailRegistrBox.Text,
                        Password = passwordRegistrBox.Password
                    });

                    MessageBox.Show("You have Registered successfully.", "", MessageBoxButton.OK, MessageBoxImage.Information);
                    LoginWindow login = new LoginWindow();
                    login.Show();
                    Close();
                }
            }
        }
    }
}
