﻿using System.Windows;
using PersonalFinanceManager.Model.Helpers;
using PersonalFinanceManager.Model.DbServices;
using PersonalFinanceManager.Model.DomainModel;

namespace PersonalFinanceManager
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>

    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void signUpButton_Click(object sender, RoutedEventArgs e)
        {
            RegistrationWindow registration = new RegistrationWindow();
            registration.Show();
            Close();
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            if (!new ValidationHelper().EmailIsValid(emailBox.Text))
            {
                MessageBox.Show("Enter a valid email.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                emailBox.Focus();
            }
            else
            {
                var userExists = new UserDbService().Login(new User()
                {
                    Email = emailBox.Text,
                    Password = passwordBox.Password
                });

                if (userExists)
                {
                    UserData.Id = new UserDbService().GetUserId(emailBox.Text);
                    WelcomeWindow welcome = new WelcomeWindow();
                    welcome.Show();
                    Close();
                }
                else
                {
                    MessageBox.Show("Sorry! Please enter existing email/password.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

    }
}
