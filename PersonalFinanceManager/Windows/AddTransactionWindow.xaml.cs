﻿using PersonalFinanceManager.Model.DomainModel;
using PersonalFinanceManager.Model.Helpers;
using PersonalFinanceManager.Model.DbServices;
using System;
using System.Windows;

namespace PersonalFinanceManager
{
    /// <summary>
    /// Interaction logic for AddTransactionWindow.xaml
    /// </summary>
    public partial class AddTransactionWindow : Window
    {
        public AddTransactionWindow()
        {
            InitializeComponent();

            RefreshCategories("Expense");
            transactionDate.SelectedDate = DateTime.Today;
        }

        private void addCategoryButton_Click(object sender, RoutedEventArgs e)
        {
            CategoryManagerWindow category = new CategoryManagerWindow();
            category.Show();
        }

        private void addTransactionButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(valueBox.Text))
            {
                MessageBox.Show("Enter a value.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                valueBox.Focus();
            }
            else if (new ValidationHelper().ValueIsValid(valueBox.Text))
            {
                MessageBox.Show("Enter a valid value.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                valueBox.Focus();
            }
            else
            {
                new TransactionDbService().Add(new Transaction()
                {
                    Date = transactionDate.SelectedDate.Value.Date,
                    CategoryId = (int)categoryComboBox.SelectedValue,
                    Value = Convert.ToDecimal(valueBox.Text),
                    Comment = commentBox.Text,
                    UserId = UserData.Id
                });
                MessageBox.Show("New transaction successfully added.", "", MessageBoxButton.OK, MessageBoxImage.Information);

            }
        }

        public void RefreshCategories(string type)
        {
            var categories = new CategoryDbService().GetAllCategory(type);
            categoryComboBox.ItemsSource = categories;
            categoryComboBox.DisplayMemberPath = "Title";
            categoryComboBox.SelectedValuePath = "Id";
            categoryComboBox.SelectedIndex = 0;
        }

        private void expenseType_Checked(object sender, RoutedEventArgs e)
        {
            RefreshCategories("Expense");
        }

        private void incomeType_Checked(object sender, RoutedEventArgs e)
        {
            RefreshCategories("Income");
        }

    }
}
