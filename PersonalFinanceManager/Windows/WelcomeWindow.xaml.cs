﻿using PersonalFinanceManager.Model.DbServices;
using PersonalFinanceManager.Model.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace PersonalFinanceManager
{
    /// <summary>
    /// Interaction logic for WelcomeWindow.xaml
    /// </summary>
    public partial class WelcomeWindow : Window
    {
        public WelcomeWindow()
        {
            InitializeComponent();

            List<string> categoriesTime = new List<string>() { "All time", "Period", "Last year", "Last month", "Last week", "Current day" };

            timeForBalanceComboBox.ItemsSource = categoriesTime;
            timeForBalanceComboBox.SelectedIndex = 5;

            timeForTransactionComboBox.ItemsSource = categoriesTime;
            timeForTransactionComboBox.SelectedIndex = 5;

            fromBalanceDate.SelectedDate = DateTime.Today;
            toBalanceDate.SelectedDate = DateTime.Today;

            fromTransactionDate.SelectedDate = DateTime.Today;
            toTransactionDate.SelectedDate = DateTime.Today;
        }

        private void RefreshBalances()
        {
            if (fromBalanceDate.SelectedDate != null && toBalanceDate.SelectedDate != null)
            {
                DateTime dateFrom = fromBalanceDate.SelectedDate.Value;
                DateTime dateTo = toBalanceDate.SelectedDate.Value;

                decimal expensesBalance = new TransactionDbService().GetBalanceByPeriod(dateFrom, dateTo, "Expense", UserData.Id);
                decimal incomesBalance = new TransactionDbService().GetBalanceByPeriod(dateFrom, dateTo, "Income", UserData.Id);

                expenseBalanceBlock.Text = expensesBalance.ToString();

                incomeBalanceBlock.Text = incomesBalance.ToString();

                balanceBlock.Text = (incomesBalance - expensesBalance).ToString();
            }
        }

        private void RefreshTransactions()
        {
            if (fromTransactionDate.SelectedDate != null && toTransactionDate.SelectedDate != null)
            {
                DateTime dateFrom = fromTransactionDate.SelectedDate.Value;
                DateTime dateTo = toTransactionDate.SelectedDate.Value;

                var transactions =
                   new TransactionDbService().GetFromPeriodTransaction(dateFrom, dateTo, UserData.Id);

                var source = new ObservableCollection<TransactionsModel>(transactions);
                transactionDataGrid.ItemsSource = source;
            }
        }

        private void timeBalanceComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DateTime dateFrom = DateTime.Today;
            DateTime dateTo = DateTime.Today;
            bool defaultMode = true;
            dateFromLabel.Content = string.Empty;
            dateToLabel.Content = string.Empty;

            switch (timeForBalanceComboBox.SelectedItem.ToString())
            {
                case "Last year":
                    fromBalanceDate.Visibility = Visibility.Collapsed;
                    toBalanceDate.Visibility = Visibility.Collapsed;

                    dateFrom = new DateTime(DateTime.Today.Year, 1, 1);
                    dateTo = new DateTime(DateTime.Today.Year, 12, 31);

                    break;
                case "All time":
                    fromBalanceDate.Visibility = Visibility.Collapsed;
                    toBalanceDate.Visibility = Visibility.Collapsed;

                    defaultMode = false;

                    DateTime? dateMin = new TransactionDbService().GetMinDate(UserData.Id);
                    DateTime? dateMax = new TransactionDbService().GetMaxDate(UserData.Id);

                    dateFromLabel.Content = dateMin == null ? string.Empty :
                        ((DateTime)dateMin).ToShortDateString();
                    dateToLabel.Content = dateMax == null ? string.Empty :
                        ((DateTime)dateMax).ToShortDateString();

                    decimal expensesBalance = new TransactionDbService().GetBalanceByType("Expense", UserData.Id);
                    decimal incomesBalance = new TransactionDbService().GetBalanceByType("Income", UserData.Id);

                    expenseBalanceBlock.Text = expensesBalance.ToString();

                    incomeBalanceBlock.Text = incomesBalance.ToString();

                    balanceBlock.Text = (incomesBalance - expensesBalance).ToString();

                    break;
                case "Period":
                    fromBalanceDate.Visibility = Visibility.Visible;
                    toBalanceDate.Visibility = Visibility.Visible;
                    
                    defaultMode = false;
                    break;
                case "Last month":
                    fromBalanceDate.Visibility = Visibility.Collapsed;
                    toBalanceDate.Visibility = Visibility.Collapsed;

                    dateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                    dateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month + 1, 1).AddDays(-1);
                    break;
                case "Last week":
                    fromBalanceDate.Visibility = Visibility.Collapsed;
                    toBalanceDate.Visibility = Visibility.Collapsed;

                    dateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).AddDays(-7);
                    dateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                    break;
                case "Current day":
                    fromBalanceDate.Visibility = Visibility.Collapsed;
                    toBalanceDate.Visibility = Visibility.Collapsed;

                    dateFrom = DateTime.Today;
                    dateTo = DateTime.Today;
                    break;
            }

            if (defaultMode)
            {
                dateFromLabel.Content = dateFrom.ToShortDateString();
                dateToLabel.Content = dateTo.ToShortDateString();

                decimal expensesBalance = new TransactionDbService().GetBalanceByPeriod(dateFrom, dateTo, "Expense", UserData.Id);
                decimal incomesBalance = new TransactionDbService().GetBalanceByPeriod(dateFrom, dateTo, "Income", UserData.Id);

                expenseBalanceBlock.Text = expensesBalance.ToString();

                incomeBalanceBlock.Text = incomesBalance.ToString();

                balanceBlock.Text = (incomesBalance - expensesBalance).ToString();
            }

        }

        private void categoryManagerButton_Click(object sender, RoutedEventArgs e)
        {
            CategoryManagerWindow categoryWindow = new CategoryManagerWindow();
            categoryWindow.Show();
        }

        private void timeForTransactionComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DateTime dateFrom = DateTime.Today;
            DateTime dateTo = DateTime.Today;
            bool defaultMode = true;
            dateFromTransactionLabel.Content = string.Empty;
            dateToTransactionLabel.Content = string.Empty;

            switch (timeForTransactionComboBox.SelectedItem.ToString())
            {
                case "Last year":
                    fromTransactionDate.Visibility = Visibility.Collapsed;
                    toTransactionDate.Visibility = Visibility.Collapsed;

                    dateFrom = new DateTime(DateTime.Today.Year, 1, 1);
                    dateTo = new DateTime(DateTime.Today.Year, 12, 31);

                    break;
                case "All time":
                    fromTransactionDate.Visibility = Visibility.Collapsed;
                    toTransactionDate.Visibility = Visibility.Collapsed;

                    defaultMode = false;

                    DateTime? dateMin = new TransactionDbService().GetMinDate(UserData.Id);
                    DateTime? dateMax = new TransactionDbService().GetMaxDate(UserData.Id);

                    dateFromTransactionLabel.Content = dateMin == null ? string.Empty :
                        ((DateTime)dateMin).ToShortDateString();
                    dateToTransactionLabel.Content = dateMax == null ? string.Empty :
                        ((DateTime)dateMax).ToShortDateString();

                    var transactions =
                    new TransactionDbService().GetAllTransaction(UserData.Id);

                    var source = new ObservableCollection<TransactionsModel>(transactions);
                    transactionDataGrid.ItemsSource = source;
                    break;
                case "Period":
                    fromTransactionDate.Visibility = Visibility.Visible;
                    toTransactionDate.Visibility = Visibility.Visible;

                    defaultMode = false;
                    break;
                case "Last month":
                    fromTransactionDate.Visibility = Visibility.Collapsed;
                    toTransactionDate.Visibility = Visibility.Collapsed;

                    dateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                    dateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month + 1, 1).AddDays(-1);
                    break;
                case "Last week":
                    fromTransactionDate.Visibility = Visibility.Collapsed;
                    toTransactionDate.Visibility = Visibility.Collapsed;

                    dateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).AddDays(-7);
                    dateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                    break;
                case "Current day":
                    fromTransactionDate.Visibility = Visibility.Collapsed;
                    toTransactionDate.Visibility = Visibility.Collapsed;

                    dateFrom = DateTime.Today;
                    dateTo = DateTime.Today;
                    break;
            }

            if (defaultMode)
            {
                dateFromTransactionLabel.Content = dateFrom.ToShortDateString();
                dateToTransactionLabel.Content = dateTo.ToShortDateString();

                var transactions =
                    new TransactionDbService().GetFromPeriodTransaction(dateFrom, dateTo, UserData.Id);

                var source = new ObservableCollection<TransactionsModel>(transactions);
                transactionDataGrid.ItemsSource = source;
            }
        }

        private void addTransactionButton_Click(object sender, RoutedEventArgs e)
        {
            AddTransactionWindow transaction = new AddTransactionWindow();
            transaction.Show();
        }

        private void MyExpenceButton_Click(object sender, RoutedEventArgs e)
        {
            MyTransactionWindow transaction = new MyTransactionWindow();
            transaction.Show();
            transaction.typeComboBox.SelectedIndex = 0;
        }

        private void MyIncomeButton_Click(object sender, RoutedEventArgs e)
        {
            MyTransactionWindow transaction = new MyTransactionWindow();
            transaction.Show();
            transaction.typeComboBox.SelectedIndex = 1;
        }

        private void fromBalanceDate_SelectedDateChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            RefreshBalances();
        }

        private void toBalanceDate_SelectedDateChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            RefreshBalances();
        }

        private void toTransactionDate_SelectedDateChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            RefreshTransactions();
        }

        private void fromTransactionDate_SelectedDateChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            RefreshTransactions();
        }
    }
}
