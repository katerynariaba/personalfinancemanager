﻿using System;

namespace PersonalFinanceManager
{
    public static class UserData
    {
        private static int id;
        private static bool idWasSet;

        public static int Id
        {
            get { return id; }
            set
            {
                if (!idWasSet)
                {
                    id = value;
                    idWasSet = true;
                }
                else
                {
                    throw new InvalidOperationException("User id can be assigned only once");
                }
            }
        }
    }
}
