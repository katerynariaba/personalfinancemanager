﻿using PersonalFinanceManager.Model.DB;
using PersonalFinanceManager.Model.DomainModel;
using PersonalFinanceManager.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalFinanceManager.Model.DbServices
{
    /// <summary>
    /// Klasa, która zawiera w sobie funkcję wspomagające pracę z kategoriami.
    /// </summary>
    public class CategoryDbService
    {
        /// <summary>
        /// Dodaje nową kategorię do bazy danych.
        /// </summary>
        /// <param name="category">Kategoria, którą dodajemy.</param>
        public void Add(Category category)
        {
            using (FinanceDbContext context = new FinanceDbContext())
            {
                context.Categories.Add(category);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Zwraca wszystkie kategorię znajdujące w bazie danych.
        /// </summary>
        /// <param name="type">String reprezentujący typ kategorii.</param>

        public List<CategoryModel> GetAllCategory(string type)
        {
            var categories = new List<CategoryModel>();
            using (FinanceDbContext context = new FinanceDbContext())
            {
                var result = context.Categories.Where(r => r.Type == type).Select(r => new CategoryModel
                {
                    Id = r.Id,
                    Title = r.Title
                });
                categories = result.Distinct().OrderBy(r => r.Title).ToList();

            }
            return categories;
        }

        /// <summary>
        /// Zwraza id podanej kategorii.
        /// </summary>
        /// <param name="title">String reprezentujący tytuł kategorii.</param>
        public int GetCategoryIdByTytle(string title)
        {
            int categoryId = 0;

            using (FinanceDbContext context = new FinanceDbContext())
            {
                categoryId = context.Categories.FirstOrDefault(r => r.Title == title).Id;
            }
            return categoryId;
        }

        /// <summary>
        /// Zwraza kategorię wegług podanego tytułu.
        /// </summary>
        /// <param name="title">String reprezentujący tytuł kategorii.</param>
        public Category GetCategoryByTitle(string title)
        {
            Category category = null;
            using (FinanceDbContext context = new FinanceDbContext())
            {
                category = context.Categories.FirstOrDefault(r => r.Title == title);
            }
            return category;
        }


        /// <summary>
        /// Usuwa kategorię z bazy danych.
        /// </summary>
        /// <param name="category">Kategoria, którą usuwamy.</param>
        public void DeleteCategory(Category category)
        {
            using (FinanceDbContext context = new FinanceDbContext())
            {
                context.Categories.Attach(category);
                context.Categories.Remove(category);
                context.SaveChanges();
            }
        }
    }
}
