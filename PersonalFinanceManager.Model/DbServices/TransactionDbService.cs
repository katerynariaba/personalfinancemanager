﻿using PersonalFinanceManager.Model.DB;
using PersonalFinanceManager.Model.DomainModel;
using PersonalFinanceManager.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PersonalFinanceManager.Model.DbServices
{
    /// <summary>
    /// Klasa, która zawiera w sobie funkcję wspomagające pracę z transakcjami.
    /// </summary>
    public class TransactionDbService
    {
        /// <summary>
        /// Dodaje nową transakcję do bazy danych.
        /// </summary>
        /// <param name="transaction">Transakcja, którą dodajemy.</param>
        public void Add(Transaction transaction)
        {
            using (FinanceDbContext context = new FinanceDbContext())
            {
                context.Transactions.Add(transaction);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Zwraca listę wszystkich transakcji w podanym zakresie czasu zrealizowane podanym użytkownikiem.
        /// </summary>
        /// <param name="from">DateTime reprezentujący początek zakresu czasu.</param>
        /// <param name="to">DateTime reprezentujący koniec zakresu czasu.</param>
        /// <param name="userId">Integer reprezentujący id użytkownika.</param>
        public List<TransactionsModel> GetFromPeriodTransaction(DateTime from, DateTime to, int userId)
        {
            var transactions = new List<TransactionsModel>();
            using (FinanceDbContext context = new FinanceDbContext())
            {
                var transactionsFromPeriod = context.Transactions.Where(r => (r.Date >= from) && (r.Date <= to) && (r.UserId == userId))
                    .ToList();

                transactions = transactionsFromPeriod.Select(r => new TransactionsModel
                {
                    Category = r.Category.Title,
                    Value = r.Value,
                    Date = r.Date.ToShortDateString(),
                    Comment = r.Comment
                }).ToList();
            }
            return transactions;
        }

        /// <summary>
        /// Zwraca listę wszystkich transakcji zrealizowane podanym użytkownikiem.
        /// </summary>
        /// <param name="to">DateTime reprezentujący koniec zakresu czasu.</param>
        /// <param name="userId">Integer reprezentujący id użytkownika.</param>
        public List<TransactionsModel> GetAllTransaction(int userId)
        {
            var transactions = new List<TransactionsModel>();
            using (FinanceDbContext context = new FinanceDbContext())
            {
                var transactionsFromPeriod = context.Transactions.Where(r => r.UserId == userId)
                    .ToList();

                transactions = transactionsFromPeriod.Select(r => new TransactionsModel
                {
                    Category = r.Category.Title,
                    Value = r.Value,
                    Date = r.Date.ToShortDateString(),
                    Comment = r.Comment
                }).ToList();
            }
            return transactions;
        }

        /// <summary>
        /// Zwraca listę wszystkich kategorii wraz z sumą wartości należącą do każdej kategorii
        /// w podanym zakresie czasu. Transakcję zrealizowane podanym użytkownikiem.
        /// </summary>
        /// <param name="from">DateTime reprezentujący początek zakresu czasu.</param>
        /// <param name="to">DateTime reprezentujący koniec zakresu czasu.</param>
        /// <param name="userId">Integer reprezentujący id użytkownika.</param>
        public List<TransactionsSummary> SummaryByCategory(DateTime from, DateTime to, string type, int userId)
        {
            var result = new List<TransactionsSummary>();
            using (FinanceDbContext context = new FinanceDbContext())
            {
                result = context.Transactions
                    .Where(r => (r.Category.Type == type) && (r.Date >= from) && (r.Date <= to) && (r.UserId == userId))
                    .GroupBy(r => r.Category.Title)
                    .Select(r => new TransactionsSummary
                    {
                        Category = r.Key,
                        Summary = r.Sum(g => g.Value)
                    }).ToList();
            }
            return result;
        }

        /// <summary>
        /// Zwraca listę wszystkich kategorii wraz z sumą wartości należącą do każdej kategorii.
        /// Transakcję zrealizowane podanym użytkownikiem.
        /// </summary>
        /// <param name="type">String reprezentujący typ kategorii.</param>
        /// <param name="userId">Integer reprezentujący id użytkownika.</param>
        public List<TransactionsSummary> SummaryAllByCategory(string type, int userId)
        {
            var result = new List<TransactionsSummary>();
            using (FinanceDbContext context = new FinanceDbContext())
            {
                result = context.Transactions.Where(r => (r.Category.Type == type) && (r.UserId == userId))
                    .GroupBy(r => r.Category.Title)
                    .Select(r => new TransactionsSummary
                    {
                        Category = r.Key,
                        Summary = r.Sum(g => g.Value)
                    }).ToList();
            }
            return result;
        }

        /// <summary>
        /// Zwraca balance podanego użytkownika według podanego zakresu czasu.
        /// </summary>
        /// <param name="from">DateTime reprezentujący początek zakresu czasu.</param>
        /// <param name="to">DateTime reprezentujący koniec zakresu czasu.</param>
        /// <param name="type">String reprezentujący typ kategorii.</param>
        /// <param name="userId">Integer reprezentujący id użytkownika.</param>
        public decimal GetBalanceByPeriod(DateTime from, DateTime to, string type, int userId)
        {
            decimal balance = 0;

            using (FinanceDbContext context = new FinanceDbContext())
            {
                balance = context.Transactions
                    .Where(r => (r.Category.Type == type) && (r.Date >= from) && (r.Date <= to) && (r.UserId == userId))
                    .Select(r => r.Value).DefaultIfEmpty(0).Sum();
            }
            return balance;
        }

        /// <summary>
        /// Zwraca balance podanego użytkownika w podanym typu kategorii.
        /// </summary>
        /// <param name="type">String reprezentujący typ kategorii.</param>
        /// <param name="userId">Integer reprezentujący id użytkownika.</param>
        public decimal GetBalanceByType(string type, int userId)
        {
            decimal balance = 0;

            using (FinanceDbContext context = new FinanceDbContext())
            {
                balance = context.Transactions
                    .Where(r => (r.Category.Type == type) && (r.UserId == userId))
                    .Select(r => r.Value).DefaultIfEmpty(0).Sum();
            }
            return balance;
        }

        /// <summary>
        /// Zwraca minimalną datę znajdującą w teansakcjach podanego użytkownika.
        /// </summary>
        /// <param name="userId">Integer reprezentujący id użytkownika.</param>
        public DateTime? GetMinDate(int userId)
        {
            DateTime? minDate = null;

            using (FinanceDbContext context = new FinanceDbContext())
            {
                minDate = context.Transactions
                    .Where(r => r.UserId == userId)
                    .Select(r => r.Date).Min();
            }
            return minDate;
        }

        /// <summary>
        /// Zwraca maksymalną datę znajdującą w teansakcjach podanego użytkownika.
        /// </summary>
        /// <param name="userId">Integer reprezentujący id użytkownika.</param>
        public DateTime? GetMaxDate(int userId)
        {
            DateTime? maxDate = null;

            using (FinanceDbContext context = new FinanceDbContext())
            {
                maxDate = context.Transactions
                    .Where(r => r.UserId == userId)
                    .Select(r => r.Date).Max();
            }
            return maxDate;
        }

        /// <summary>
        /// Zwraca transakcję wegług podanych krytegii.
        /// </summary>
        /// <param name="userId">Integer reprezentujący id użytkownika.</param>
        /// <param name="categoryId">Integer reprezentujący id kategorii.</param>
        /// <param name="value">Decimal reprezentujący wartość transakcji.</param>
        /// <param name="date">DateTime reprezentujący datę realizowanie transakcji.</param>
        public Transaction GetTransaction(int categoryId, int userId, decimal value, DateTime date)
        {
            Transaction transaction = null;
            using (FinanceDbContext context = new FinanceDbContext())
            {
                transaction = context.Transactions
                    .FirstOrDefault(r => (r.CategoryId == categoryId) && (r.UserId == userId)
                    && (r.Value == value) && (r.Date == date));
            }
            return transaction;
        }


        /// <summary>
        /// Usuwa transakcję z bazy danych.
        /// </summary>
        /// <param name="transaction">Transakcja, którą usuwamy.</param>
        public void DeleteTransaction(Transaction transaction)
        {
            using (FinanceDbContext context = new FinanceDbContext())
            {
                context.Transactions.Attach(transaction);
                context.Transactions.Remove(transaction);
                context.SaveChanges();
            }
        }
    }
}
