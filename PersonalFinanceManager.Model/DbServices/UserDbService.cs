﻿using PersonalFinanceManager.Model.DB;
using PersonalFinanceManager.Model.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalFinanceManager.Model.DbServices
{
    /// <summary>
    /// Klasa, która zawiera w sobie funkcję wspomagające pracę z użytkownikami.
    /// </summary>
    public class UserDbService
    {
        /// <summary>
        /// Dodaje nowrgo użytkownika do bazy danych.
        /// </summary>
        /// <param name="user">Użytkownik, którego dodajemy.</param>
        public void Add(User user)
        {
            using (FinanceDbContext context = new FinanceDbContext())
            {
                context.Users.Add(user);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Sprawdza czy dany użytkownik już jest w bazie danych.
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>True dane użytkownika już znajdują się w bazie danych, inaczej False.</returns>
        public bool Login(User user)
        {
            using (FinanceDbContext context = new FinanceDbContext())
            {
                if (context.Users.Any(p => p.Email == user.Email && p.Password == user.Password))
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Zwraca id użytkownika za podanym email.
        /// </summary>
        /// <param name="email">Sring reprezentujący email użytkownika</param>
        public int GetUserId(string email)
        {
            int userId = 0;

            using (FinanceDbContext context = new FinanceDbContext())
            {
                userId = context.Users.FirstOrDefault(r => r.Email == email).Id;
            }

            return userId;
        }


        /// <summary>
        /// Usuwa użytkownika z bazy danych.
        /// </summary>
        /// <param name="user">Użytkownik, którego usuwamy.</param>
        public void DeleteUser(User user)
        {
            using (FinanceDbContext context = new FinanceDbContext())
            {
                context.Users.Attach(user);
                context.Users.Remove(user);
                context.SaveChanges();
            }
        }
    }
}
