namespace PersonalFinanceManager.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init5 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Costs", newName: "Transactions");
            DropForeignKey("dbo.Revenues", "UserId", "dbo.Users");
            DropIndex("dbo.Revenues", new[] { "UserId" });
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Transactions", "CategoryID", c => c.Int(nullable: false));
            AddColumn("dbo.Transactions", "TransactionDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Transactions", "Value", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            CreateIndex("dbo.Transactions", "CategoryID");
            AddForeignKey("dbo.Transactions", "CategoryID", "dbo.Categories", "Id", cascadeDelete: true);
            DropColumn("dbo.Transactions", "CostDate");
            DropColumn("dbo.Transactions", "Category");
            DropTable("dbo.Revenues");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Revenues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        RevenueDate = c.DateTime(nullable: false),
                        Category = c.String(),
                        Value = c.Double(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Transactions", "Category", c => c.String());
            AddColumn("dbo.Transactions", "CostDate", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.Transactions", "CategoryID", "dbo.Categories");
            DropIndex("dbo.Transactions", new[] { "CategoryID" });
            AlterColumn("dbo.Transactions", "Value", c => c.Double(nullable: false));
            DropColumn("dbo.Transactions", "TransactionDate");
            DropColumn("dbo.Transactions", "CategoryID");
            DropTable("dbo.Categories");
            CreateIndex("dbo.Revenues", "UserId");
            AddForeignKey("dbo.Revenues", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            RenameTable(name: "dbo.Transactions", newName: "Costs");
        }
    }
}
