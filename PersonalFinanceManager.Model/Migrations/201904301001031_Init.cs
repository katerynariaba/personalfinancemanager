namespace PersonalFinanceManager.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Costs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        CostDate = c.DateTime(nullable: false),
                        Category = c.String(),
                        Value = c.Int(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LastName = c.String(),
                        FirstName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Revenues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        RevenueDate = c.DateTime(nullable: false),
                        Category = c.String(),
                        Value = c.Int(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Revenues", "UserId", "dbo.Users");
            DropForeignKey("dbo.Costs", "UserId", "dbo.Users");
            DropIndex("dbo.Revenues", new[] { "UserId" });
            DropIndex("dbo.Costs", new[] { "UserId" });
            DropTable("dbo.Revenues");
            DropTable("dbo.Users");
            DropTable("dbo.Costs");
        }
    }
}
