namespace PersonalFinanceManager.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init6 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Transactions", new[] { "CategoryID" });
            CreateIndex("dbo.Transactions", "CategoryId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Transactions", new[] { "CategoryId" });
            CreateIndex("dbo.Transactions", "CategoryID");
        }
    }
}
