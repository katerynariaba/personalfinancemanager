﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PersonalFinanceManager.Model.Helpers
{
    /// <summary>
    /// Klasa, która zawiera w sobie funkcję sprawdzające walidność.
    /// </summary>
    public class ValidationHelper
    {
        /// <summary>
        /// Sprawdza czy podany email jest walidny.
        /// </summary>
        /// <param name="email">String reprezentujący email.</param>
        /// <returns>True jeśli email jest walidny, inaczej False.</returns>
        public bool EmailIsValid(string email)
        {
            if (Regex.IsMatch(email, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
                return true;

            return false;
        }

        /// <summary>
        /// Sprawdza czy podana wartość jest liczbą.
        /// </summary>
        /// <param name="value">String reprezentujący wartość.</param>
        /// <returns>True jeśli podana wartość jest liczbą, inaczej False.</returns>
        public bool ValueIsValid(string value)
        {
            if (Regex.IsMatch(value, @"^[^0-9.-]+"))
                return true;

            return false;
        }
    }
}
