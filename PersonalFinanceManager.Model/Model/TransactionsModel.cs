﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalFinanceManager.Model.Model
{
    /// <summary>
    /// Klasa reprezentująca model transakcji, zawierająca tytuł kategorii.
    /// </summary>
    public class TransactionsModel
    {
        public string Category { get; set; }
        public string Date { get; set; }
        public decimal Value { get; set; }
        public string Comment { get; set; }
    }
}
