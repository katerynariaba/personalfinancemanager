﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalFinanceManager.Model.Model
{
    /// <summary>
    /// Klasa reprezentująca model transakcji,
    /// zawierająca tytuł kategorii oraz wartość wszystkich transakcji w danej kategorii.
    /// </summary>
    public class TransactionsSummary
    {
        public string Category { get; set; }
        public decimal Summary { get; set; }
    }
}
