﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalFinanceManager.Model.Model
{
    /// <summary>
    /// Klasa reprezentująca model kategorii,zawierająca id kategorii oraz tytuł.
    /// </summary>
    public class CategoryModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
