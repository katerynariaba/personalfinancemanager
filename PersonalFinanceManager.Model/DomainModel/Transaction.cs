﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalFinanceManager.Model.DomainModel
{
    /// <summary>
    /// Klasa reprezentująca model transakcji.
    /// </summary>
    public class Transaction
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int CategoryId { get; set; }
        public DateTime Date { get; set; }
        public decimal Value { get; set; }
        public string Comment { get; set; }

        public virtual User User { get; set; }
        public virtual Category Category { get; set; }       
    }
}
