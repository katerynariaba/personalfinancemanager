﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalFinanceManager.Model.DomainModel
{
    /// <summary>
    /// Klasa reprezentująca model kategorii.
    /// </summary>
    public class Category
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }

    }
}
