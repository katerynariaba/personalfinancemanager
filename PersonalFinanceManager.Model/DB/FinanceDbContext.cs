﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PersonalFinanceManager.Model.DomainModel;

namespace PersonalFinanceManager.Model.DB
{
    /// <summary>
    /// Klasa, która reprezentuje DbContext.
    /// </summary>
    public class FinanceDbContext : DbContext
    {

        public FinanceDbContext() : base(@"Data Source=DESKTOP-5MLMB9J;Initial Catalog=FinanceManagerDb;Integrated Security=True")
        {

        }
        
        public DbSet<User> Users { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
    }
}
